@extends('layout.main')

@section('content')
    <!--banner Section starts Here -->
			<div class="bannercontainer spacetop">
				<div class="banner">
					<ul>
						<!-- THE BOXSLIDE EFFECT EXAMPLES  WITH LINK ON THE MAIN SLIDE EXAMPLE -->

						<li data-transition="random" data-slotamount="1">
							<img src="assets/images/banner-plane.jpg" alt="" />
							<div class="banner-text">
								<div class="caption sft big_white" data-x="0" data-y="100" data-speed="center" data-start="1700" data-easing="Power4.easeInOut">
									<a href="#" class="shipping">ground shipping</a>
								</div>
								<div class="caption sfb big_orange clearfix"  data-x="100" data-y="350" data-speed="500" data-start="1900" data-easing="Power4.easeInOut">
									<h2>ONE STOP SOLUTION
									YOUR TRANSPORT
									REQUIREMENTS</h2>
								</div>
								<div class="caption lfr medium_grey"  data-x="left" data-y="center" data-speed="300" data-start="2000">
									<a href="service.html" class="services-link">our services</a>
								</div>
							</div>
						</li>
						<li data-transition="random" data-slotamount="1">
							<img src="assets/images/banner-train.jpg" alt="" />
							<div class="banner-text">
								<div class="caption sft big_white" data-x="0" data-y="100" data-speed="700" data-start="1700" data-easing="Power4.easeInOut">
									<a href="#" class="shipping">ground shipping</a>
								</div>
								<div class="caption sfb big_orange clearfix"  data-x="100" data-y="350" data-speed="500" data-start="1900" data-easing="Power4.easeInOut">
									<h2>ONE STOP SOLUTION
									YOUR TRANSPORT
									REQUIREMENTS</h2>
								</div>
								<div class="caption lfr medium_grey" data-x="left" data-y="center" data-speed="300" data-start="2000">
									<a href="service.html" class="services-link">our services</a>
								</div>
							</div>
						</li>
						<li data-transition="random" data-slotamount="1">
							<img src="assets/images/banner-truck.jpg" alt="" />
							<div class="banner-text">
								<div class="caption sft big_white" data-x="0" data-y="100" data-speed="700" data-start="1700" data-easing="Power4.easeInOut">
									<a href="#" class="shipping">ground shipping</a>
								</div>
								<div class="caption sfb big_orange clearfix"  data-x="100" data-y="350" data-speed="500" data-start="1900" data-easing="Power4.easeInOut">
									<h2>ONE STOP SOLUTION
									YOUR TRANSPORT
									REQUIREMENTS</h2>
								</div>
								<div class="caption lfr medium_grey"  data-x="left" data-y="center" data-speed="300" data-start="2000">
									<a href="service.html" class="services-link">our services</a>
								</div>
							</div>
						</li>

					</ul>
				</div>
			</div>
			<!--banner Section ends Here -->
@endsection
