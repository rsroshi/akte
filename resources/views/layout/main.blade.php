<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Transport</title>
    <link rel="shortcut icon" type="image/png" href="favicon.png">

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Karla:400,700italic,700,400italic' rel='stylesheet' type='text/css'>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" />

    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="assets/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="assets/css/dropdown.css" />

    <link rel="stylesheet" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" href="assets/css/global.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/responsive.css" />
    <link href="assets/css/skin.less" rel="stylesheet/less">

</head>

<body>

    <!-- Loader Starts here -->
    <div class="loader-block">
        <div class="loader">
            Loading...
        </div>
    </div>
    <!-- Loader Ends here -->
    <div id="wrapper" class="homepage homepage-1">

        @include('layout.header')

        @yield('content')


        @include('layout.footer')
    </div>


</body>

