<!--Footer area starts Here -->
<footer id="footer">
    <!--Footer box starts Here -->
    <div class="footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="quick-links">
                        <h5>QUICK LINKS</h5>
                    </div>
                    <div class="quick-list">
                        <ul>
                            <li>
                                <a href="#">ground shipping</a>
                            </li>
                            <li>
                                <a href="#">air freight</a>
                            </li>
                            <li>
                                <a href="#">sea freight</a>
                            </li>
                            <li>
                                <a href="#">storage &amp; packaging</a>
                            </li>
                        </ul>
                    </div>
                    <div class="quick-list">
                        <ul>
                            <li>
                                <a href="#">rail shipping</a>
                            </li>
                            <li>
                                <a href="#">logistic solutions</a>
                            </li>
                            <li>
                                <a href="#">cargo shipping</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="our-address">
                        <h5>contact us</h5>
                        <div class="address">
                            <h6>transport king</h6>
                            <address>
                                Contact Address will appear here, some text gonna
                                appear here, Ney York City, - 10001, USA
                            </address>
                            <div class="phone">
                                <span>phone : <a href="tel:5917890123">591 7890 123</a></span>
                                <span>email : <a href="mail.html"><span class="__cf_email__" data-cfemail="c0a9aea6af80b4b2a1aeb3b0afb2b4b4a8a5ada5eea3afad">[email&#160;protected]</span></a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="sign-up">
                        <h5>newsletter signup</h5>
                        <p>
                            If you want receive our all weekly updates about new
                            offers and discount, signup below.
                        </p>
                        <form>
                            <input id="mail" type="text" placeholder="Email Address" name="email" />
                            <!-- <input id="submit" type="submit" value="" class="fa fa-paper-plane-o" /> -->
                            <button class="fa fa-paper-plane"></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row custom-row">
                <div class="col-xs-12 col-sm-5">
                    <div class="copyright">
                        <span>Copyright 2015. All Rights Reserved by <a href="#">Transport.</a></span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 no-wrap-mobile">
                    <div class="footer-nav">
                        <ul>
                            <li>
                                <a href="#">terms of use</a>
                            </li>
                            <li>
                                <a href="#">legal desclaimer</a>
                            </li>
                            <li>
                                <a href="#">privacy policy</a>
                            </li>
                            <li>
                                <a href="#">support</a>
                            </li>
                            <li>
                                <a href="#">sitemap</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Footer box ends Here -->
</footer>
<!--Footer area ends Here -->




<script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.selectbox-0.2.min.js"></script>

<!--Parrallax -->
<script type="text/javascript" src="assets/js/parallax.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/revolution.js"></script>

<!-- Switcher Js -->
<script src="assets/js/theme-option/style-switcher/assets/js/style.switcher.js"></script>
<script src="assets/js/theme-option/style-switcher/assets/js/jquery.cookie.js"></script>
<!-- Switcher Js -->

<script src="assets/js/theme-option/style-switcher/assets/js/less.js"></script>
<script src="assets/js/script.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/js/site.js"></script>
